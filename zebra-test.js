const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const { spawn } = require('child_process');
const path = require('path');
const fs = require('fs');
const crypto = require('crypto');

const body = {
  printerIP: '172.16.13.54:80',
  pngData: [
    {
      png: fs.readFileSync('./label.png', {encoding: 'base64'}),
      quantity: 2,
    }
  ]
}

async function printLabelsFromPngController({printerIP, pngData}) {
	const zpl = [];
	for (let i = 0; i < pngData.length; i += 1) {
		const png = pngData[i].png;
		const quantity = pngData[i].quantity;
		const zplImage = await convertPNG(png);
		for (let j = 0; j < quantity; j += 1) zpl.push(`^XA^FO0,0${zplImage}^XZ`);
	}

	const request = new XMLHttpRequest();
  request.open("POST", `http://${printerIP}/pstprnt`, true); 
	request.send(zpl.join(''));
}

function convertPNG(pngData) {
	return new Promise((resolve, reject) => {
		const fileName = `./${crypto.randomBytes(20).toString('hex')}.png`;
		fs.writeFileSync(fileName, pngData, {encoding: 'base64'})

		const ls = spawn('java', ['-jar', path.resolve('./lib/ImgToZPL.jar'), fileName]);
		let stdout = [], stderr = [];
		ls.stdout.on('data', (data) => {
			stdout.push(data.toString());
		});
		ls.stderr.on('data', (data) => {
			stderr.push(data.toString());
		});
		ls.on('close', (code) => {
			fs.unlinkSync(fileName)
			if (stderr.length > 0) {
				reject(stderr.join(''))
			} else {
				resolve(stdout.join(''))
			}
		});
	});
}

printLabelsFromPngController(body);
